// MongoDB Operations
// For creating or inserting data into database
// input object
db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
        },
        
    courses: ["CSS", "Javascript", "Phyton"],
    department: "none"
})

//Insert many documents
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "53477123",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "5347345",
			email: "francisM@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

//==========================================

// for querying all the data in database
// for finding documents

// find all
db.users.find();

// find single
db.users.find({firstName: "Francis", age: 61}); 

//==========================================

// delete single documents
db.users.deleteOne({
    firstName: "Ely"
});

// delete many documents
// delete many with the same details
db.users.deleteMany({
    department: "DOH"
});

//==========================================

//updating documents on DB
//$set plays important role this is where we change the details

//updates single
db.users.updateOne(
    {
        firstName: "Gloc-8"
    },
    {
        $set: {
            department: "DOH"
            }
    }
); 

//updates many
// you can update it with the same details on the target eg. department: "none"

db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);